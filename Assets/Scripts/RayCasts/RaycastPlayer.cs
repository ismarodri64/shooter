﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastPlayer : MonoBehaviour
{
    
    //Lo usare para indicarle el script recoil que estoy disparando
    public event EventHandler playershooting;
    public delegate void shot(GameObject victim);
    public static event shot OnIAShot;
    bool canIShoot = true;
    //Indicas si tienes municion
    public static bool noAmmo = false;

    [SerializeField(), Range(0f, 1000f)]
    float range;

    //La fuerza que dara al rigidbody que recibio el disparo del raycast.
    public float force= 100f;
    //true si quieres ver la distancia que tiene el raycast en cuanto colisionas con alguien (lo entenderas si lo pruebas). Edit: como solo imprimira el raycast al disparar, lo comentaremos porque no se nota mucho.
    //public bool printDistanceHit;

    // Update is called once per frame
    void Update()
    {

        
        //Si no tiene hijos (osea si esta desarmado) significa que no esta usando ahora ninguna arma y no vale la pena usar el raycast.
        //Por que hago esto??? Para reutilizar codigo y para que sea compatible en caso de cambio de escenas. Si lo hago instanciando con un gameobject publico pues, ya sabes que pasa al recargar escena.
        if (this.transform.parent.transform.GetChild(2).transform.childCount != 0 && !noAmmo)
        {
            //Depende de la arma, disparara de una diferente forma.
            fireMode(this.transform.parent.transform.GetChild(2).transform.GetChild(0).gameObject.name);
        }
        

        
       

    }

    private void fireMode(string name)
    {
        if (name.Contains("Rifle"))
        {
            if (Input.GetMouseButton(0))
            {
                shotsFired(this.transform.parent.transform.GetChild(2).transform.GetChild(0).gameObject.name);
            }
            
        }
        else
        {
            //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * rc.distance, Color.red);
            
            
            if (Input.GetMouseButtonDown(0))
            {

                //Igual que lo anterior pero cojo solo su nombre
                shotsFired(this.transform.parent.transform.GetChild(2).transform.GetChild(0).gameObject.name);

            }
        }
    }






    //Actual weapon es el nombre de la arma que actualmente esta usando, segun su nombre del gameobject.
    private void shotsFired(String actualweapon)
    {
        
        if (actualweapon.Contains("Pistola"))
        {
            simple();
            playershooting?.Invoke(this, EventArgs.Empty);

        }
        else if (actualweapon.Contains("Shotgun"))
        {
            perdigones();
            playershooting?.Invoke(this, EventArgs.Empty);
        }
        else
        {
            StartCoroutine(tyler1MachineGun());
        }
        
        
    }

    





    //Disparos normales y corrientes, los tipicos disparos de una pistola o una M1 Garand por ejemplo
    private void simple()
    {
        RaycastHit hit;
        //Origen (Gameobject), direccion del raycast (teoricamente coge el rotation en caso de que escoges otro gameobject), out hit es un simple output, range el rango, y el 3 es la mascara que vas a ignorar, el 3 es un layer vacio que puedes usar para no ignorar nada.
        if (Physics.Raycast(transform.position, transform.forward, out hit, range, 3))
        {

            if (hit.collider.gameObject.tag == "Enemy")
            {
                if (hit.collider.gameObject.GetComponent<LifeMono>() != null)
                {
                    //Restara la vida del enemigo
                    Debug.Log("Hit Point " + hit.point + " Scale y " + hit.transform.localScale.y);
                    //Lo que hace es calcular el point "y" (la "y" donde el gameobject recibio el disparo), y la escala "y" del gameobject, multiplicado por 150. Cuanto mas alto le des, mas daño. Asi hace como un efecto de que has dado headshot
                    //Edit: lo hemos quitado por tema de bugs
                    //int headShot = (int)((hit.point.y / hit.transform.localScale.y) * 150);
                    int rng = UnityEngine.Random.Range(0, 100);
                    Debug.Log("Total= " + rng);
                    hit.collider.gameObject.GetComponent<LifeMono>().life -= rng;
                }
                else
                {
                    Debug.LogWarning("You have an gameobject that have a tag Enemy but you don't have the script ''LifeMono''.");
                }

                if (OnIAShot != null)
                {
                    OnIAShot(hit.collider.gameObject);
                }
            }

            //if (printDistanceHit) Debug.DrawRay(transform.position, transform.forward * hit.distance, Color.yellow);
            //else Debug.DrawRay(transform.position, transform.forward * range, Color.yellow);
            Debug.DrawRay(transform.position, transform.forward * range, Color.yellow);

            //AddForceAtPosition(fuerza,position); La fuerza tienes que ponerle el hit.normal porque le tienes que dar coordenadas del mundo, y la cantidad es la multiplicacion que hace.

            if (hit.collider.gameObject.GetComponent<Rigidbody>() != null)
            {
                hit.transform.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * force, hit.point);
                Debug.Log("Did Hit " + hit.collider);
            }


        }
        else
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * range, Color.white);
            //Debug.Log("Did not Hit");
        }
    }




    //Disparos randoms que depende de tu suerte o de lo cerca que estes dañe mas. Resumen, escopetas.
    private void perdigones()
    {
        
        int perdigones = 5;


        for(int i= 0; i < perdigones; i++)
        {
            //Cojo los 3 vectors del forward y les pongo un random.
            Vector3 rngVector= new Vector3(transform.forward.x + UnityEngine.Random.Range(-0.25f, 0.25f), transform.forward.y + UnityEngine.Random.Range(-0.25f, 0.25f), transform.forward.z + UnityEngine.Random.Range(-0.25f, 0.25f));
            RaycastHit hit;
            //Origen (Gameobject), direccion del raycast (teoricamente coge el rotation en caso de que escoges otro gameobject), out hit es un simple output, range el rango, y el 3 es la mascara que vas a ignorar, el 3 es un layer vacio que puedes usar para no ignorar nada.
            if (Physics.Raycast(transform.position, rngVector, out hit, range/12, 3))
            {

                if (hit.collider.gameObject.tag == "Enemy")
                {
                    if (hit.collider.gameObject.GetComponent<LifeMono>() != null)
                    {
                        //Restara la vida del enemigo
                        Debug.Log("Hit Point " + hit.point + " Scale y " + hit.transform.localScale.y);
                        
                        //int headShot = (int)((hit.point.y / hit.transform.localScale.y) * 150);
                        int rng = UnityEngine.Random.Range(0, 100);
                        Debug.Log("Total= " + rng);
                        hit.collider.gameObject.GetComponent<LifeMono>().life -= rng;
                    }
                    else
                    {
                        Debug.LogWarning("You have an gameobject that have a tag Enemy but you don't have the script ''LifeMono''.");
                    }

                    if (OnIAShot != null)
                    {
                        OnIAShot(hit.collider.gameObject);
                    }
                }

                //if (printDistanceHit) Debug.DrawRay(transform.position, transform.forward * hit.distance, Color.yellow);
                //else Debug.DrawRay(transform.position, transform.forward * range, Color.yellow);
                Debug.DrawRay(transform.position, rngVector * range/12, Color.yellow);

                //AddForceAtPosition(fuerza,position); La fuerza tienes que ponerle el hit.normal porque le tienes que dar coordenadas del mundo, y la cantidad es la multiplicacion que hace.


                if (hit.collider.gameObject.GetComponent<Rigidbody>() != null)
                {
                    hit.transform.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * force, hit.point);
                    Debug.Log("Did Hit " + hit.collider);
                }



            }
            else
            {
                //transform.TransformDirection(Vector3.forward)
                Debug.DrawRay(transform.position, rngVector * range/12, Color.white);
                //Debug.Log("Did not Hit");
            }
        }
    }

    //Igual que simple pero con cooldown.
    IEnumerator tyler1MachineGun()
    {
        if (canIShoot)
        {
            
            canIShoot= false;
            simple();
            playershooting?.Invoke(this, EventArgs.Empty);
            yield return new WaitForSeconds(0.1f);
            canIShoot = true;
            
        }
        
    }


}
