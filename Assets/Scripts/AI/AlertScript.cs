﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlertScript : MonoBehaviour
{
    RaycastHit hit;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.name == "Ping" || other.gameObject.tag == "Player")
        {
            if (Physics.Raycast(this.gameObject.transform.position, (other.gameObject.transform.position - this.gameObject.transform.position), out hit))
            {
                if(hit.collider.name == "Ping" || hit.collider.gameObject.tag == "Player") 
                {
                    this.gameObject.transform.parent.gameObject.GetComponent<AIStatus>().alert = true;
                }
                
            }
                  
        }
        if(other.gameObject.tag == "Enemy" && this.gameObject.transform.parent.GetComponent<AIStatus>().alert && Vector3.Distance(this.gameObject.transform.position, other.gameObject.transform.position) < 20)
        {
            
            if (!other.gameObject.GetComponent<AIStatus>().alert)
            {
                other.gameObject.GetComponent<AIStatus>().alert = true;
            }
        }
       
    }
}
