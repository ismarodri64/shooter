﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Patrol : ScriptableObject
{
    public Vector3 point1;
    public Vector3 point2;
    public bool patrolling;
}
