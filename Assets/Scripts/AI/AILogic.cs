﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AILogic : MonoBehaviour
{
    GameObject player;
    public Patrol path;

    bool moved = false;

    RaycastHit LOS;
    public AIType type;


    public delegate void shot(int damage);
    public static event shot PlayerShot;

    bool shotAvaliable = true;
    float shotTimer;

    bool muzzleflash = false;
    
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        RaycastPlayer.OnIAShot += Shot;
        shotTimer = 60 / type.fireRate;
    }

    // Update is called once per frame
    void Update()
    {

        if (!muzzleflash)
        {
            this.gameObject.GetComponent<Light>().range = 0;
        }
        else
        {
            this.gameObject.GetComponent<Light>().range = 15;
        }

        if(Vector3.Distance(this.gameObject.transform.position, this.gameObject.GetComponent<NavMeshAgent>().destination) < 1.5)
        {
            if (moved)
            {
                moved = false;
            }
            else
            {
                moved = true;
            }
            
        }

        if (!this.GetComponent<AIStatus>().alert) {
            if (path.patrolling)
            {
                if (!moved && this.gameObject.GetComponent<NavMeshAgent>().destination != path.point1)
                {
                    this.gameObject.GetComponent<NavMeshAgent>().destination = path.point1;
                }
                if (moved && this.gameObject.GetComponent<NavMeshAgent>().destination != path.point2)
                {
                    this.gameObject.GetComponent<NavMeshAgent>().destination = path.point2;
                }
            }
        }
        else
        {
            if (this.GetComponent<AIStatus>().alert)
            {
                Physics.Raycast(this.gameObject.transform.position, (player.gameObject.transform.position - this.gameObject.transform.position), out LOS);
                if (type.maxRange < Vector3.Distance(this.gameObject.transform.position, player.transform.position) || LOS.collider.tag != "Player")
                {
                    this.gameObject.GetComponent<NavMeshAgent>().destination = player.transform.position;
                }
                else
                {
                    this.gameObject.GetComponent<NavMeshAgent>().destination = this.gameObject.transform.position;
                    if (shotAvaliable)
                    {
                        int shotChance = Random.Range(1, 101);
                        StartCoroutine(MuzzleFlash());
                        if (shotChance >= type.missRate)
                        {


                            if (PlayerShot != null)
                            {
                                PlayerShot(type.hitDamage);
                                shotAvaliable = false;
                                StartCoroutine(ShotInnerTimer());
                            }
                        }
                        else
                        {
                            shotAvaliable = false;
                            StartCoroutine(ShotInnerTimer());
                        }
                    }
                    this.transform.LookAt(player.transform.position);
                }
                     
            }
        }
        
    }

    void Shot(GameObject victim)
    {
        if(victim.gameObject == this.gameObject)
        {
            if (!this.gameObject.GetComponent<AIStatus>().alert)
            {
                this.gameObject.GetComponent<AIStatus>().alert = true;
            }
        }
    }
    IEnumerator ShotInnerTimer()
    {
        yield return new WaitForSeconds(shotTimer);
        shotAvaliable = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player" && type.melee)
        {
            PlayerShot(type.hitDamage);
        }
    }

    IEnumerator MuzzleFlash() {
        muzzleflash = true;
        yield return new WaitForSeconds(0.1f);
        muzzleflash = false;
    }
}
