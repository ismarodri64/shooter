﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class AIType : ScriptableObject
{
    public string type;
    public bool melee;
    public int maxRange;
    public float fireRate;
    public int hitDamage;
    public int missRate;

}
