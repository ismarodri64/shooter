﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Reload : MonoBehaviour
{

    RaycastPlayer rcp;

    int ammoInside = 30;
    Text AmmoInd;
    void Start()
    {
        AmmoInd = GameObject.Find("Ammo").GetComponent<Text>();
        rcp = GameObject.Find("FirstPersonCamera").GetComponent<RaycastPlayer>();
        rcp.playershooting += Rcp_playershooting;
    }

    private void Rcp_playershooting(object sender, System.EventArgs e)
    {

        if (ammoInside > 0)
        {
            ammoInside--;
        }
        Debug.Log("Ammo: " + ammoInside);
    }

    // Update is called once per frame
    void Update()
    {
        if (ammoInside <= 0)
        {
            //Debug.Log("Empty. Reload with R");
            AmmoInd.text = "Empty. Reload with R";
            //Es un estatico, asi que no habra problemas y asi no requerira el uso de un evento para solo 1 funcion y etc.
            //Si no hay balas, no puedes disparar
            RaycastPlayer.noAmmo = true;
        }
        else
        {
            AmmoInd.text = "Ammo " + ammoInside;
            //Si hay balas, puedes disparar
            RaycastPlayer.noAmmo = false;
        }


        StartCoroutine(reloading());


    }

    IEnumerator reloading()
    {
        if (Input.GetKeyDown("r"))
        {
            //AmmoInd.text = "Reloading";
            yield return new WaitForSeconds(2f);
            ammoInside = 30;
        }
    }
}
