﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mirar : MonoBehaviour
{

    public static bool cursorBloqueado = true;

    public Transform player;
    public Transform cams;
    public Transform arma;

    public float xSensitivity;
    public float ySensitivity;
    public float maxAngulo;

    private Quaternion camCentro;

    
    void Start()
    {
        //origen de rotacion para las cameras
        camCentro = cams.localRotation;
    }

    
    void Update()
    {
        SetY();
        SetX();

        ActualizarCursorBloqueado();
    }

    void SetY()
    {
        float input = Input.GetAxis("Mouse Y") * ySensitivity * Time.deltaTime;
        // ap es ajuste personal, lo pongo aqui para no olvidarme del nombre.
        // se pone en negativo por lo de girar a la derecha y restarle puntos
        Quaternion ap = Quaternion.AngleAxis(input, -Vector3.right);
        //este eje es el Y 
        Quaternion delta = cams.localRotation * ap;

        if(Quaternion.Angle(camCentro, delta) < maxAngulo)
        {
            cams.localRotation = delta;
            arma.localRotation = delta;
        }

        arma.rotation = cams.rotation;
    }


    void SetX()
    {
        float input = Input.GetAxis("Mouse X") * xSensitivity * Time.deltaTime;
        // ap es ajuste personal, lo pongo aqui para no olvidarme del nombre.
        // aqui es en positivo para girar hacia arriba 
        Quaternion ap = Quaternion.AngleAxis(input, Vector3.up);
        Quaternion delta = player.localRotation * ap;
        player.localRotation = delta;
    }

    void ActualizarCursorBloqueado()
    {
        if (cursorBloqueado)
        {
            //El bloqueo del punto del cursor es igual al bloqueo del cursor
            //Que no sea visible el cursor
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                cursorBloqueado = false;
            }
        }
        else
        {
            //El bloqueo del punto del cursor es igual al bloqueo del cursor que es nulo en este caso
            //Que sea visible el cursor
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                cursorBloqueado = true;
            }
        }

    }
}
