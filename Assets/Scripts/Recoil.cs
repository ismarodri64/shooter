﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recoil : MonoBehaviour
{
    RaycastPlayer rcp;

    
    float recoil= 5f;

    public bool IsAWeapon = false;

    void Start()
    {
        //Tiene que estar en el mismo jugador
        //rcp = this.GetComponent<RaycastPlayer>();
        //Se que es raro. Teoricamente la camara tendra el raycastplayer del jugador
        rcp = GameObject.Find("FirstPersonCamera").GetComponent<RaycastPlayer>();
        rcp.playershooting += Rcp_playershooting;
    }

    private void Rcp_playershooting(object sender, EventArgs e)
    {
        //Debug.Log("Shooting");
        float angle = this.transform.eulerAngles.x;
        //El child 2 es el gameobject arma i miraremos si tiene hijos o no.
        if (!IsAWeapon && this.transform.GetChild(2).transform.childCount !=0)
        {
            if (angle > 180)
            {
                angle = angle - 360;
            }
            if (angle > -30)
            {
                this.transform.Rotate(-recoil, 0, 0);
            }
        }
        //Solo va a entrar si el gameobject se considera una arma y tiene hijos
        else if (IsAWeapon && this.transform.childCount != 0)
        {
            //localposition para que no le afecte al padre (las armas son hijos del player)
            this.transform.GetChild(0).transform.localPosition = new Vector3(0, 0, -1f);
            //this.transform.localPosition = new Vector3(0, 0, -1f);
        }
        
        
        
        //StartCoroutine(restartingrotation());
        
    }

    

    void Update()
    {
        rotationTo0();
        restartingrotation();
    }

    private void restartingrotation()
    {
        //Reinicio la rotacion porque hay un bug que hace como rotar la "y" y la "z" del jugador al disparar.
        this.transform.eulerAngles = new Vector3(this.transform.eulerAngles.x, this.transform.eulerAngles.y, 0);
    }



    private void rotationTo0()
    {
        if (!IsAWeapon)
        {
            //Debug.Log(angle);
            if (this.transform.eulerAngles.x != 0)
            {
                this.transform.Rotate(0.45f, 0, 0);
                if (this.transform.eulerAngles.x <= 10)
                {
                    this.transform.eulerAngles = new Vector3(0, this.transform.eulerAngles.y, this.transform.eulerAngles.z);
                }
            }
        }
        //Las cosas cambian si es una arma que contiene hijos
        else if(IsAWeapon && this.transform.childCount!=0)
        {
            //Debug.Log(this.transform.localPosition.z);
            if (this.transform.GetChild(0).transform.localPosition.z < 0)
            {
                this.transform.GetChild(0).transform.localPosition = new Vector3(0, 0, this.transform.GetChild(0).transform.localPosition.z+0.05f);
            }
        }
    }
}
