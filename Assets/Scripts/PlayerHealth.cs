﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{

    public static PlayerHealth singleton;
    public float currentHealth;
    public float maxHealth = 100f;
    public Slider healthSlider;
    public Text healthContador;
    public bool isDead = false;


    private void Awake()
    {
        singleton = this;
    }

    void Start()
    {
        healthContador = GameObject.Find("Health Contador").GetComponent<Text>();
        healthSlider = GameObject.Find("HealthSlider").GetComponent<Slider>();
        AILogic.PlayerShot += PlayerDamage;
        currentHealth = maxHealth;
        healthSlider.value = maxHealth;
        UpdateHealthContador();
    }


    public void PlayerDamage(int damage)
    {
       if(currentHealth > 0)
        {
            if (damage >= currentHealth)
            {
                Dead();
            }
            else
            {
                currentHealth -= damage;
                healthSlider.value -= damage;
            }
            UpdateHealthContador();
        }
    }

    void Dead()
    {
        currentHealth = 0;
        isDead = true;
        healthSlider.value = 0;
        UpdateHealthContador();
        AILogic.PlayerShot -= PlayerDamage;
        Debug.Log("Has muerto");
        SceneManager.LoadScene("GameOver");
        
    }


    void UpdateHealthContador()
    {
        healthContador.text = currentHealth.ToString();
    }
}
